# Plausible Page Report

Create a CSV of all page views by URL. Plausible has a [page stats export](https://plausible.io/docs/export-stats), but this silently limits itself to the 100 most viewed pages. It does however have an [API](https://plausible.io/docs/stats-api#get-apiv1statsbreakdown).

## Usage

Create an API key, and export it as `$PLAUSIBLE_API_TOKEN`

```
python3 report.py <hostname>
```

If you run your own Plausible instance ([like me](https://theorangeone.net/posts/self-hosting-plausible/)), set `$PLAUSIBLE_HOST`.
