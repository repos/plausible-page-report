import requests
import argparse
import os
import datetime
from itertools import count
import csv

PLAUSIBLE_HOSTNAME = os.environ.get("PLAUSIBLE_HOSTNAME", "plausible.io")


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("site_id", type=str)

    return parser.parse_args()


def get_pages(site_id: str, session: requests.Session):
    today = datetime.datetime.now().date().isoformat()

    for page_num in count(start=1):
        print("Fetching page", page_num)
        response = session.get(f"https://{PLAUSIBLE_HOSTNAME}/api/v1/stats/breakdown", params={
            "site_id": site_id,
            "period": "custom",
            "date": "2000-01-01," + today,
            "property": "event:page",
            "page": page_num,
            "metrics": "pageviews,visitors"
        })
        response.raise_for_status()

        results = response.json()["results"]
        if not results:
            break

        yield from results


def main():
    args = parse_args()

    session = requests.Session()
    session.headers["Authorization"] = "Bearer " + os.environ["PLAUSIBLE_API_TOKEN"]

    with open(f"report-{args.site_id}.csv", "w") as f:
        writer = csv.DictWriter(f, fieldnames=["page", "visitors", "pageviews"])

        for page in get_pages(args.site_id, session):
            writer.writerow(page)


if __name__ == "__main__":
    main()
